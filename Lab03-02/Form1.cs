﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab03_02
{
    public partial class Form1 : Form
    {
        FontConverter cvt = new FontConverter();
        public Form1()
        {
            InitializeComponent();
            foreach (FontFamily font in new InstalledFontCollection().Families)
            {
               
                cmbFonts.Items.Add(font.Name);
               
            }
            cmbFonts.SelectedIndex = 10;
            for(int i = 9; i <= 72; i ++)
            {
                toolStripComboBox2.Items.Add(i.ToString());
            }
            toolStripComboBox2.SelectedIndex = 3;
        }

        private void toolStripComboBox1_Click(object sender, EventArgs e)
        {

        }
        private void ResetFormat()
        {
            richText.Font = cvt.ConvertFromString("Time new roman") as Font;
            
            richText.ForeColor = Color.Black;
            richText.SelectionFont = new Font("Tahoma", 12, FontStyle.Regular);
        }
        private void địnhDạngToolStripMenuItem_Click(object sender, EventArgs e)
        {
                FontDialog fontDlg = new FontDialog();
                fontDlg.ShowColor = true;
                fontDlg.ShowApply = true;
                fontDlg.ShowEffects = true;
                fontDlg.ShowHelp = true;
                if (fontDlg.ShowDialog() != DialogResult.Cancel)
                {
                    richText.ForeColor = fontDlg.Color;
                    richText.Font = fontDlg.Font;
                }
        }

        private void tạoVănBảnMớiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetFormat();
            richText.Text = "";
        }

        private void cbSize_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripComboBox2_TextChanged(object sender, EventArgs e)
        {
            int size = Int16.Parse(toolStripComboBox2.Text);
            string font = cmbFonts.SelectedText.ToString();
            richText.Font = new Font(font, size);
        }

        private void cmbFonts_TextChanged(object sender, EventArgs e)
        {
            string font = cmbFonts.SelectedText.ToString();
            richText.Font = cvt.ConvertFromString(font) as Font;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ResetFormat();
            this.richText.Text = "";
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            SaveFile();
            MessageBox.Show("Lưu file thành công");
        }
        private void SaveFile()
        {
            SaveFileDialog SaveFileDialog1 = new SaveFileDialog();
            SaveFileDialog1.ShowDialog();
            richText.SaveFile(SaveFileDialog1.FileName);
        }
        private void hệThôngToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void lưuNộiDungVănBảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
            MessageBox.Show("Lưu file thành công");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            
            richText.Font = new Font(richText.SelectionFont, FontStyle.Bold);
            
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            richText.Font = new Font(richText.SelectionFont, FontStyle.Italic);
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            Font font = richText.Font;
            richText.Font = new Font(richText.SelectionFont, FontStyle.Underline);
        }

        private void mởTậpTinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            //lọc hiện thị các loại file
            //dlg.Filter = "Text file| *.txt | MPEG File | *.mpeg | Wav File |*.Wav | Midi File | *.midi | Mp4 File | *.mp4 | MP3 | *.mp3";
            if (dlg.ShowDialog() == DialogResult.OK)
                this.richText.LoadFile(dlg.FileName);
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
