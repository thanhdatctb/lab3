﻿using Lab3._3.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3._3
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
            this.dtgvStudent.DataSource = Session.students;
            this.dtgvStudent.Columns["maSv"].HeaderText = "Mã sinh viên";
            this.dtgvStudent.Columns["tenSv"].HeaderText = "tên sinh viên";
            this.dtgvStudent.Columns["khoa"].HeaderText = "Khoa";
            this.dtgvStudent.Columns["diem"].HeaderText = "Điểm trung bình";
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            new AddNew().Show();
            this.Hide();
        }

        private void thêmMớiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            new AddNew().Show();
            this.Hide();
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Search(string keyword)
        {
            List<Student> searchStd = new List<Student>();
            foreach(var student in Session.students)
            {
                if(student.tenSv.IndexOf(keyword)!= -1)
                {
                    searchStd.Add(student);
                }    
            }
            this.dtgvStudent.DataSource = searchStd;
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string keyword = this.txtSearch.Text;
            Search(keyword);
        }
    }
}
