﻿using Lab3._3.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3._3
{
    public partial class AddNew : Form
    {
        public AddNew()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            new Form1().Show();
        }
        private bool checkId(string id)
        {
            foreach(var student in Session.students)
            {
                if (student.maSv.Equals(id))
                {
                    return false;
                }    
            }
            return true;
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            Student student = new Student();
            if (String.IsNullOrEmpty(txtMaSv.Text) ||
                String.IsNullOrEmpty(txtTenSv.Text) ||
                String.IsNullOrEmpty(txtDiem.Text) ||
                String.IsNullOrEmpty(cbKhoa.Text))
            {
                MessageBox.Show("Phải điền hết các trường");
                return;
            } 
            if(!checkId(this.txtMaSv.Text))
            {
                MessageBox.Show("Trùng Mã sinh viên");
                return;
            }  
            try
            {
                
                student.maSv = this.txtMaSv.Text;
                student.tenSv = this.txtTenSv.Text;
                student.khoa = cbKhoa.Text;
                student.diem = Int16.Parse(this.txtDiem.Text);
            
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            if(student.diem > 10 || student.diem <0)
            {
                MessageBox.Show("Điểm phải từ 0 đến 10");
                return;
            }    
            Session.students.Add(student);
            button1_Click(sender,  e);
        }
    }
}
