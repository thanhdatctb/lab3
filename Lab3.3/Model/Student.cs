﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3._3.Model
{
    public class Student
    {
        public string maSv { get; set; }
        public string tenSv { get; set; }
        public string khoa { get; set; }
        public float diem { get; set; }
    }
}
