﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3._4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnFrom_Click(object sender, EventArgs e)
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            // Always default to Folder Selection.
            folderBrowser.FileName = "Folder Selection";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                txtFrom.Text = folderPath; //đưa txt nguồn = folderPath chọn
            }

        }

        private void BtnTo_Click(object sender, EventArgs e)
        {
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            // Always default to Folder Selection.
            folderBrowser.FileName = "Folder Selection";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                txtTo.Text = folderPath; //đưa txt nguồn = folderPath chọn
            }

        }

        private void btnCoppy_Click(object sender, EventArgs e)
        {
            string msg = "Đang sao chép: {0}";
            if (Directory.Exists(txtFrom.Text) && Directory.Exists(txtTo.Text))
                
 {
                string[] files = Directory.GetFiles(txtFrom.Text);
                int step = 100 / files.Count();
                foreach (string f in files)
                {
                    string fileName = Path.GetFileName(f);
                    string desFile = Path.Combine(txtTo.Text, fileName);
                    File.Copy(f, desFile);
                    progressBar1.Value += step;
                    msg = string.Format(msg, Path.Combine(txtFrom.Text, fileName));
                    LoadStatusStrip(msg);
                }
            }

        }
        void LoadStatusStrip(string status)
        {
            this.statusStrip1.Text = status;
            ToolTip tooltip = new ToolTip();
            tooltip.SetToolTip(progressBar1, status);
        }
    }
}
